
:Begin:
:Function:       addtwo
:Pattern:        AddTwo[i_Integer, j_Integer]
:Arguments:      { i, j }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:
:Evaluate: AddTwo::usage = "AddTwo[x, y] gives the sum of two machine integers x and y."


:Begin:
:Function:       multiplytwo
:Pattern:        MultiplyTwo[i_Integer, j_Integer]
:Arguments:      { i, j }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:
:Evaluate: MultiplyTwo::usage = "MultiplyTwo[x, y] gives the product of two machine integers x and y."


:Begin:
:Function:       summation
:Pattern:        Summation[a_List]
:Arguments:      { a }
:ArgumentTypes:  { IntegerList }
:ReturnType:     Integer
:End:
:Evaluate: Summation::usage = "Summation[a] gives the total sum of the integers list a."


:Begin:
:Function:       last_sum
:Pattern:        LastSum[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Integer
:End:
:Evaluate: LastSum::usage = "LastSum[] gives the total sum according to last call to Summation[]"


