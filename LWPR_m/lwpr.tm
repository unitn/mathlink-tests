
:Begin:
:Function:       version
:Pattern:        LWPRVersion[ ]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

:Begin:
:Function:       init_model
:Pattern:        LWPRInitModel[nIn_Integer, nOut_Integer, name_String]
:Arguments:      { nIn, nOut, name }
:ArgumentTypes:  { Integer, Integer, String }
:ReturnType:     Manual
:End:
:Evaluate: LWPRInitModel::usage = "LWPRInitModel[nIn, nOut, name] Initialises an LWPR model and allocates internally used storage for submodels etc.\n\nParams:\nnIn:\tthe number of input dimensions\nnOut:\tthe number of output dimensions\nname:\tthe model name"


:Begin:
:Function:       set_real_field
:Pattern:        LWPRSetModelField[n_String, v_Real]
:Arguments:      { n, v }
:ArgumentTypes:  { String, Real }
:ReturnType:     Manual
:End:

:Begin:
:Function:       set_integer_field
:Pattern:        LWPRSetModelField[n_String, v_Integer]
:Arguments:      { n, v }
:ArgumentTypes:  { String, Integer }
:ReturnType:     Manual
:End:

:Begin:
:Function:       set_string_field
:Pattern:        LWPRSetModelField[n_String, v_String]
:Arguments:      { n, v }
:ArgumentTypes:  { String, Integer }
:ReturnType:     Manual
:End:
:Evaluate: LWPRSetModelField::usage = "LWPRSetModelField[name, value] Set a model field to the given value."


:Begin:
:Function:       get_model_field
:Pattern:        LWPRModelField[n_String]
:Arguments:      { n }
:ArgumentTypes:  { String }
:ReturnType:     Manual
:End:
:Evaluate: LWPRModelField::usage = "LWPRModelField[name] Get a model field."





:Begin:
:Function:       set_init_D_spherical
:Pattern:        LWPRSetInitDSpherical[s_Real]
:Arguments:      { s }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:
:Evaluate: LWPRSetInitDSpherical::usage = "LWPRSetInitDSpherical[s] Set a diagonal initial distance metric for creating new receptive fields.\n\nParams:\nsigma:\tScale parameter of the distance metric, larger values imply narrower kernels"


:Begin:
:Function:       set_init_alpha
:Pattern:        LWPRSetInitAlpha[a_Real]
:Arguments:      { a }
:ArgumentTypes:  { Real }
:ReturnType:     Manual
:End:
:Evaluate: LWPRSetInitAlpha::usage = "LWPRSetInitAlpha[a] Set a spherical initial distance metric for creating new receptive fields.\n\nParams:\nalpha:\tScalar learning rate (the same for all elements of the distance metric)"


:Begin:
:Function:       set_init_D_diagonal
:Pattern:        LWPRSetInitDDiagonal[d_List]
:Arguments:      { d }
:ArgumentTypes:  { RealList }
:ReturnType:     Manual
:End:
:Evaluate: LWPRSetInitDDiagonal::usage = "LWPRSetInitDDiagonal[d] Set initial distance metric for creating new receptive fields.\n\nParams:\nd\tDiagonal elements of the distance metric (must be >= 0). d must point to an array of nIn doubles"


:Begin:
:Function:       set_init_D
:Pattern:        LWPRSetInitD[d_List, stride_Integer]
:Arguments:      { d, stride }
:ArgumentTypes:  { RealList, Integer }
:ReturnType:     Manual
:End:
:Evaluate: LWPRSetInitD::usage = "LWPRSetInitD[d, stride] Set initial distance metric for creating new receptive fields.\n\nParams:\nd\tSymmetric, positive definite distance metric. Must point to an array of at least nIn*stride doubles.\nstride\tOffset between the first element of different columns of D. Pass nIn if the matrix is stored densely, that is, without any space between adjacent columns."


:Begin:
:Function:       predict
:Pattern:        LWPRPredict[x_List, cutoff_Real]
:Arguments:      { x, cutoff }
:ArgumentTypes:  { RealList, Real}
:ReturnType:     Manual
:End:
:Evaluate: LWPRPredict::usage = "LWPRPredict[x, cutoff] Computes the prediction of an LWPR model given an input vector x. Can also return confidence bounds and the maximal activation of all receptive fields.\n\nParams:\nx\tInput vector, an array of nIn doubles\ncutoff\tA threshold parameter. Receptive fields with activation below the cutoff are ignored\n\nReturns a list with three entries:\ny\tOutput vector, an array of nOut doubles\nconf\tConfidence bounds per output dimension, an array of nOut doubles\nmax_w\tMaximum activation per output dimension, an array of nOut doubles"


:Begin:
:Function:       predict_J
:Pattern:        LWPRPredictJ[x_List, cutoff_Real]
:Arguments:      { x, cutoff, y, J }
:ArgumentTypes:  { RealList, Real }
:ReturnType:     Manual
:End:
:Evaluate: LWPRPredictJ::usage = "LWPRPredictJ[x, cutoff ] Computes the prediction and its derivatives (Jacobian) of an LWPR model given an input vector x.\n\nParams:\nx\tInput vector, an array of nIn doubles\ncutoff\tA threshold parameter. Receptive fields with activation below the cutoff are ignored\n\nReturns a list with two entries:\ny\tOutput vector, an array of nOut doubles\nJ\tJacobian matrix, i.e. derivatives of output vector with respect to input vector, an array of nOut*nIn doubles. The matrix is stored in column-major order"


:Begin:
:Function:       predict_JcJ
:Pattern:        LWPRPredictJcJ[x_List, cutoff_Real]
:Arguments:      { x, cutoff }
:ArgumentTypes:  { RealList, Real }
:ReturnType:     Manual
:End:
:Evaluate: LWPRPredictJcJ::usage = "LWPRPredictJcJ[x, cutoff] Computes the predictions, the confidence intervals and the first derivatives of all the quantities.\n\nParams:\nx\tInput vector, an array of nIn doubles\ncutoff\tA threshold parameter. Receptive fields with activation below the cutoff are ignored\n\nReturns a list with four entries:\ny\tOutput vector, an array of nOut doubles\nJ\tJacobian matrix, i.e. derivatives of output vector with respect to input vector, an array of nOut*nIn doubles.\nconf\tConfidence intervals, an array of nOut doubles\nJconf\tJacobian of the confidences, an array of nOut*nIn"


:Begin:
:Function:       predict_JH
:Pattern:        LWPRPredictJH[x_List, cutoff_Real]
:Arguments:      { x, cutoff }
:ArgumentTypes:  { RealList, Real }
:ReturnType:     Manual
:End:
:Evaluate: LWPRPredictJH::usage = "LWPRPredictJH[x, cutoff] Computes the prediction and its first and second derivatives of an LWPR model given an input vector x.\n\nParams:\nx\tInput vector, an array of nIn doubles\ncutoff\tA threshold parameter. Receptive fields with activation below the cutoff are ignored\n\nReturns a list with three entries:\ny\tOutput vector, an array of nOut doubles\nJ\tJacobian matrix, i.e. derivatives of output vector with respect to input vector, an array of nOut*nIn doubles.\nH\tHessian matrices, i.e. 2nd derivatives of output vector with respect to input vector, an array of nIn*nIn*nOut doubles."


:Begin:
:Function:       update
:Pattern:        LWPRUpdate[x_List, y_List]
:Arguments:      { x, y }
:ArgumentTypes:  { RealList, RealList }
:ReturnType:     Manual
:End:
:Evaluate: LWPRUpdate::usage = "LWPRUpdate[x, y] Updates an LWPR model with a given input/output pair (x,y). Returns the model's prediction for y and the maximal activation of all receptive fields.\n\nParams:\nx\tInput vector, an array of nIn doubles\ny\tOutput vector, an array of nOut doubles\n\nReturns a list with two entries:\nyp\tCurrent prediction given x, an array of nOut doubles\nmax_w\tMaximum activation per output dimension, an array of nOut doubles"


:Begin:
:Function:       free_model
:Pattern:        LWPRFreeModel[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
:Evaluate: LWPRFreeModel::usage = "LWPRFreeModel[]"



