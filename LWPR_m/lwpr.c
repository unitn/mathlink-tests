//
//  lwpr.c
//  ML Tests
//
//  Created by Paolo Bosetti on 10/02/14.
//  Copyright (c) 2014 Paolo Bosetti. All rights reserved.
//

#include "mathlink.h"
#include "versioning.h"
#include <lwpr.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

LWPR_Model *_model;

void version() {
  MLPutString(stdlink, GIT_VERSION);
}

void MLPrintF(const char * fmt, ...) {
  char buf[1024] = "", str[1050] = "";
  va_list aptr;
  va_start(aptr, fmt);
  strcat(str, "Print[\"");
  vsprintf(buf, fmt, aptr);
  strcat(str, buf);
  strcat(str, "\"]");
  MLEvaluateString(stdlink, str);
  va_end(aptr);
}

#define CHECK_MODEL if (_model == NULL) { \
    MLPrintF("Call LWPRInitModel first!"); \
    MLPutSymbol(stdlink, "False"); \
    return; \
  }


#pragma mark - INITIALIZATIONS

void init_model(int nIn, int nOut, const char *name)
{
  _model = malloc(sizeof(LWPR_Model));
  if (_model == NULL || !lwpr_init_model(_model, nIn, nOut, name))
    MLPutSymbol(stdlink, "False");
  else
    MLPutSymbol(stdlink, "True");
  return;
}

void set_init_alpha(double alpha)
{
  CHECK_MODEL;
  MLPutSymbol(stdlink, lwpr_set_init_alpha(_model, alpha) == 1 ? "True" : "False");
}

void set_init_D_spherical(double sigma)
{
  CHECK_MODEL;
  MLPutSymbol(stdlink, lwpr_set_init_D_spherical(_model, sigma) == 1 ? "True" : "False");
}

void set_init_D_diagonal(double *d, int n)
{
  CHECK_MODEL;
  if (n != _model->nIn) {
    MLPrintF("Error: Argument must be an array of nIn (%d) values", _model->nIn);
    MLPutSymbol(stdlink, "False");
    return;
  }
  MLPutSymbol(stdlink, lwpr_set_init_D_diagonal(_model, d) == 1 ? "True" : "False");
  return;
}

void set_init_D(const double *D, int n, int stride)
{
  CHECK_MODEL;
  if (n != _model->nIn * stride) {
    MLPrintF("Error: Must be an array of nIn*stride (%d) values", _model->nIn * stride);
    MLPutSymbol(stdlink, "False");
    return;
  }
  MLPutSymbol(stdlink, lwpr_set_init_D(_model, D, stride) == 1 ? "True" : "False");
  return;
}

void free_model()
{
  CHECK_MODEL;
  lwpr_free_model(_model);
  free(_model);
  MLPrintF("Model removed");
  MLPutSymbol(stdlink, "True");
}



#pragma mark - PREDICTIONS

void predict(const double *x, int n_x,
             double cutoff)
{
  CHECK_MODEL;
  double y[_model->nOut];
  double conf[_model->nOut];
  double max_w[_model->nOut];
  int dims[1] = {_model->nOut};

  lwpr_predict(_model, x, cutoff, y, conf, max_w);
  
  MLPutFunction(stdlink, "List", 3);
  MLPutReal64Array(stdlink, y,     dims, NULL, 1);
  MLPutReal64Array(stdlink, conf,  dims, NULL, 1);
  MLPutReal64Array(stdlink, max_w, dims, NULL, 1);
}

void predict_J(const double *x, int n_x,
               double cutoff)
{
  CHECK_MODEL;
  double y[_model->nOut];
  double J[_model->nOut*_model->nIn];
  int dims_Y[1] = {_model->nOut};
  int dims_J[2] = {_model->nOut, _model->nIn};
  
  lwpr_predict_J(_model, x, cutoff, y, J);
  
  MLPutFunction(stdlink, "List", 2);
  MLPutReal64Array(stdlink, y, dims_Y, NULL, 1);
  MLPutReal64Array(stdlink, J, dims_J, NULL, 2);
}

void predict_JcJ(const double *x, int n_x,
                 double cutoff)
{
  CHECK_MODEL;
  double y[_model->nOut];
  double J[_model->nOut*_model->nIn];
  double conf[_model->nOut];
  double Jconf[_model->nOut*_model->nIn];
  int dims_Y[1] = {_model->nOut};
  int dims_J[2] = {_model->nOut, _model->nIn};
  
  lwpr_predict_JcJ(_model, x, cutoff, y, J, conf, Jconf);
  
  MLPutFunction(stdlink, "List", 4);
  MLPutReal64Array(stdlink, y,     dims_Y, NULL, 1);
  MLPutReal64Array(stdlink, J,     dims_J, NULL, 2);
  MLPutReal64Array(stdlink, conf,  dims_Y, NULL, 1);
  MLPutReal64Array(stdlink, Jconf, dims_J, NULL, 2);
}

void predict_JH(const double *x, int n_x,
                double cutoff)
{
  CHECK_MODEL;
  double y[_model->nOut];
  double J[_model->nOut*_model->nIn];
  double H[_model->nOut*_model->nIn*_model->nIn];
  int dims_Y[1] = {_model->nOut};
  int dims_J[2] = {_model->nOut, _model->nIn};
  int dims_H[3] = {_model->nOut, _model->nIn, _model->nIn};
  
  lwpr_predict_JH(_model, x, cutoff, y, J, H);
  
  MLPutReal64Array(stdlink, y, dims_Y, NULL, 1);
  MLPutReal64Array(stdlink, J, dims_J, NULL, 2);
  MLPutReal64Array(stdlink, H, dims_H, NULL, 3);
}

void update(const double *x, int n_x, const double *y, int n_y)
{
  CHECK_MODEL;
  double y_predict[_model->nOut];
  double max_w[_model->nOut];
  int dims[1] = {_model->nOut};
  
  lwpr_update(_model, x, y, y_predict, max_w);

  MLPutFunction(stdlink, "List", 2);
  MLPutReal64Array(stdlink, y_predict, (int *)dims, (char **)0, 1);
  MLPutReal64Array(stdlink, max_w,     (int *)dims, (char **)0, 1);
}





#pragma mark - STRUCT ACCESS

void set_real_field(const char *name, double value)
{
  CHECK_MODEL;

  unsigned long len = strlen(name);
  if (strncmp(name, "meta_rate", len) == 0) {
    _model->meta_rate = value;
    goto return_point;
  }
  else if (strncmp(name, "penalty", len) == 0) {
    _model->penalty = value;
    goto return_point;
  }
  else if (strncmp(name, "w_gen", len) == 0) {
    _model->w_gen = value;
    goto return_point;
  }
  else if (strncmp(name, "w_prune", len) == 0) {
    _model->w_prune = value;
    goto return_point;
  }
  else if (strncmp(name, "init_lambda", len) == 0) {
    _model->init_lambda = value;
    goto return_point;
  }
  else if (strncmp(name, "final_lambda", len) == 0) {
    _model->final_lambda = value;
    goto return_point;
  }
  else if (strncmp(name, "tau_lambda", len) == 0) {
    _model->tau_lambda = value;
    goto return_point;
  }
  else if (strncmp(name, "add_threshold", len) == 0) {
    _model->add_threshold = value;
    goto return_point;
  }

return_point:
  MLPutDouble(stdlink, value);
  return;
}


void set_integer_field(const char *name, long value)
{
  CHECK_MODEL;
  
  unsigned long len = strlen(name);
  if (strncmp(name, "nIn", len) == 0) {
    _model->nIn = (int)value;
    goto return_point;
  }
  else if (strncmp(name, "nOut", len) == 0) {
    _model->nOut = (int)value;
    goto return_point;
  }
  else if (strncmp(name, "n_data", len) == 0) {
    _model->n_data = (int)value;
    goto return_point;
  }
  else if (strncmp(name, "diag_only", len) == 0) {
    _model->diag_only = (int)value;
    goto return_point;
  }
  
return_point:
  MLPutLongInteger(stdlink, value);
  return;
}


void set_string_field(const char *name, char *value)
{
  CHECK_MODEL;

  unsigned long len = strlen(name);

  if (strncmp(name, "name", len) == 0) {
    _model->name = value;
    goto return_point;
  }
  
return_point:
  MLPutString(stdlink, value);
  return;
}


void get_model_field(const char *name)
{
  CHECK_MODEL;
  
  // Integer
  if (strncmp(name, "nIn", 3) == 0) {
    MLPutInteger(stdlink, _model->nIn);
  }
  else if (strncmp(name, "nOut", 4) == 0) {
    MLPutInteger(stdlink, _model->nOut);
  }
  else if (strncmp(name, "n_data", 6) == 0) {
    MLPutInteger(stdlink, _model->n_data);
  }
  else if (strncmp(name, "diag_only", 9) == 0) {
    MLPutInteger(stdlink, _model->diag_only);
  }
  else if (strncmp(name, "numRFS", 6) == 0) {
    MLPutInteger(stdlink, _model->sub->numRFS);
  }
  // Double
  else if (strncmp(name, "meta_rate", 9) == 0) {
    MLPutDouble(stdlink, _model->meta_rate);
  }
  else if (strncmp(name, "penalty", 7) == 0) {
    MLPutDouble(stdlink, _model->penalty);
  }
  else if (strncmp(name, "w_gen", 5) == 0) {
    MLPutDouble(stdlink, _model->w_gen);
  }
  else if (strncmp(name, "w_prune", 7) == 0) {
    MLPutDouble(stdlink, _model->w_prune);
  }
  else if (strncmp(name, "init_lambda", 11) == 0) {
    MLPutDouble(stdlink, _model->init_lambda);
  }
  else if (strncmp(name, "final_lambda", 12) == 0) {
    MLPutDouble(stdlink, _model->final_lambda);
  }
  else if (strncmp(name, "tau_lambda", 10) == 0) {
    MLPutDouble(stdlink, _model->tau_lambda);
  }
  else if (strncmp(name, "add_threshold", 13) == 0) {
    MLPutDouble(stdlink, _model->add_threshold);
  }
  // Array
  else if (strncmp(name, "rf.D", 4) == 0) {
    int dims[2] = {_model->nIn, _model->nIn};
    MLPutFunction(stdlink, "List", _model->sub->numRFS);
    for (int i = 0; i < _model->sub->numRFS; i++) {
      MLPutReal64Array(stdlink, _model->sub->rf[i]->D, dims, NULL, 2);
    }
  }
  else if (strncmp(name, "rf.c", 4) == 0) {
    int dims[1] = {_model->nIn};
    MLPutFunction(stdlink, "List", _model->sub->numRFS);
    for (int i = 0; i < _model->sub->numRFS; i++) {
      MLPutReal64Array(stdlink, _model->sub->rf[i]->c, dims, NULL, 1);
    }
  }
  // String
  else if (strncmp(name, "name", 4) == 0) {
    MLPutString(stdlink, _model->name);
  }
  else {
    MLPrintF("Cannot get this field.");
    MLPrintF("Valid fields are: nIn, nOut, n_data, diag_only, numRFS, meta_rate, penalty, w_gen, w_prune, init_lambda, final_lambda, tau_lambda, add_threshold, rf.D, rf.c, name.");
    MLPutSymbol(stdlink, "False");
  }
  return;
}


int main(int argc, char* argv[])
{
	return MLMain(argc, argv);
}
