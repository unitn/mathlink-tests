 #--------------------------------------------------------------------------#
 #  file: Train_Data.rb                                                     #
 #                                                                          #
 #  version: 1.0   date 12/2/2014                                           #
 #                                                                          #
 #  Copyright (C) 2014                                                      #
 #                                                                          #
 #      Enrico Bertolazzi and Francesco Biral and Paolo Bosetti             #
 #      Dipartimento di Ingegneria Industriale                              #
 #      Universita` degli Studi di Trento                                   #
 #      Via Mesiano 77, I-38050 Trento, Italy                               #
 #      email: enrico.bertolazzi@unitn.it                                   #
 #             francesco.biral@unitn.it                                     #
 #             paolo.bosetti@unitn.it                                       #
 #--------------------------------------------------------------------------#

# possible choices
# U_QUADRATIC, U_QUADRATIC2, U_PARABOLA, U_LOGARITHMIC, U_COS_LOGARITHMIC, U_HYPERBOLIC, U_CUBIC

# possible choices
# P_REGULAR, P_SMOOTH, P_PIECEWISE

include Mechatronix

# user defined values
ubMax             = 2
uaMax             = 10
ubControlMaxValue = ubMax
uaControlMaxValue = uaMax
uaControlMinValue = 0
ubControlMinValue = 0
x_i               = 0
x_f               = 6
v_i               = 0
v_f               = 0
alpha             = 0.3
beta              = 0.14
gm                = 0.16
uaMin             = 0
ubMin             = 0
epsi_max          = 0.1e-1
epsi_min          = 0.1e-3
tol_max           = 0.1e-1
tol_min           = 0.1e-2


mechatronix do |data|

  # Level of message
  data.InfoLevel = 4
  
  # setup solver
  data.Solver = {
    :max_iter  => 300,  
    :tolerance => 1e-9
  }
  
  # Boundary Conditions
  data.BoundaryConditions = {
    :initial_x => SET,
    :initial_v => SET,
    :final_x   => SET,
    :final_v   => SET,
  }

  # Model Parameters
  data.Parameters = {
    :alpha             => alpha,
    :beta              => beta,
    :epsi_max          => epsi_max,
    :epsi_min          => epsi_min,
    :gm                => gm,
    :tol_max           => tol_max,
    :tol_min           => tol_min,
    :uaControlMaxValue => uaControlMaxValue,
    :uaControlMinValue => uaControlMinValue,
    :uaMax             => uaMax,
    :uaMin             => uaMin,
    :ubControlMaxValue => ubControlMaxValue,
    :ubControlMinValue => ubControlMinValue,
    :ubMax             => ubMax,
    :ubMin             => ubMin,
    :v_f               => v_f,
    :v_i               => v_i,
    :x_f               => x_f,
    :x_i               => x_i,
  }

  # Controls
  data.Controls = {
    :uaControl => {
      :type      => U_COS_LOGARITHMIC,
      :epsilon   => epsi_max,
      :tolerance => tol_max,
    },
    :ubControl => {
      :type      => U_COS_LOGARITHMIC,
      :epsilon   => epsi_max,
      :tolerance => tol_max,
    },
  }

  data.Constraints = {
    # Constraint1D


    # Constraint2D

  }

  # M E S H
  data.Mesh = Mesh.new ( 0 )
  data.Mesh << { :length => 0.25, :n => 25 }
  data.Mesh << { :length => 0.75, :n => 3000 }
  data.Mesh << { :length => 3.8, :n => 100 }

end

# user classes initializations
# EOF
