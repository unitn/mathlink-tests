
:Begin:
:Function:       load_model
:Pattern:        MXLoadModel[ fileName_String, name_String ]
:Arguments:      { NotebookDirectory[] <> fileName, name }
:ArgumentTypes:  { String, String }
:ReturnType:     Manual
:End:
:Evaluate: MXLoadModel::usage = "MXLoadModel[fileName_String, name_String] Load model library with the given name"

:Begin:
:Function:       load_data
:Pattern:        MXLoadData[fileName_String]
:Arguments:      { NotebookDirectory[] <> fileName }
:ArgumentTypes:  { String }
:ReturnType:     Manual
:End:
:Evaluate: MXLoadData::usage = "MXLoadData[fileName_String] Load model data from a ruby file"

:Begin:
:Function:       get_data
:Pattern:        MXGetData[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
:Evaluate: MXGetData::usage = "MXGetData[] Return a list of active settings for the current model"

:Begin:
:Function:       set_data
:Pattern:        MXSetData[ list_List ]
:Arguments:      { list }
:ArgumentTypes:  { Manual }
:ReturnType:     Manual
:End:
:Evaluate: MXSetData::usage = "MXSetData[list_List] Load a list of model settings into the solver"

:Begin:
:Function:       solve
:Pattern:        MXSolve[ ]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
:Evaluate: MXSolve::usage = "MXSolve[] Solve the problem. Return True on success"

:Begin:
:Function:       get_solution
:Pattern:        MXGetSolution[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
:Evaluate: MXGetSolution::usage = "MXGetSolution[] Return the solution"

:Begin:
:Function:       write_solution
:Pattern:        MXWriteSolution[ fileName_String ]
:Arguments:      { NotebookDirectory[] <> fileName }
:ArgumentTypes:  { String }
:ReturnType:     Manual
:End:
:Evaluate: MXWriteSolution::usage = "MXWriteSolution[fileName_String] Write the solution table to fileName"

:Begin:
:Function:       describe_data
:Pattern:        MXDescribeData[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:
:Evaluate: MXDescribeData::usage = "MXDescribeData[a] Describe the current data structure"

