//
//  lpwr.c
//  ML Tests
//
//  Created by Paolo Bosetti on 10/02/14.
//  Copyright (c) 2014 Paolo Bosetti. All rights reserved.
//
#include "mathlink.h"
#include <exception>

#include "mechatronix.hh"

#pragma mark - Definitions

extern "C" {
  typedef void (*OCP_solve)( char const id[], GC::GenericContainer & gc_data, GC::GenericContainer & gc_solution );
  typedef void (*OCP_write)( char const id[], char const fname[] );
}

typedef struct {
  void             *lib_handle;
  GenericContainer  data;
  GenericContainer  solution;
  OCP_solve         solve;
  OCP_write         write;
  MrubyInterpreter  mrb;
} OCP_object;

OCP_object _ocp;


#pragma mark - Wrapper functions

void load_model(char const *name, char const *model_name)
{
  MLPutFunction(stdlink, "List", 3);
  
  // close the library link if it is already loaded
  if (_ocp.lib_handle) {
    dlclose(_ocp.lib_handle);
  }
  
  // open the library link
  MLPutFunction(stdlink, "Rule", 2);
  MLPutSymbol(stdlink, "libName");
  _ocp.lib_handle = dlopen(name, RTLD_LAZY);
  if (_ocp.lib_handle)
    MLPutString(stdlink, name);
  else
    MLPutSymbol(stdlink, "False");
  
  // connect with the solve function
  MLPutFunction(stdlink, "Rule", 2);
  MLPutSymbol(stdlink, "solve");
  string fun_name = string(model_name) + "_ocp_solve";
  _ocp.solve = (OCP_solve) dlsym(_ocp.lib_handle, fun_name.c_str());
  if (dlerror() == NULL)
    MLPutString(stdlink, fun_name.c_str());
  else
    MLPutSymbol(stdlink, "False");

  // connect with the write_solution_to_file function
  MLPutFunction(stdlink, "Rule", 2);
  MLPutSymbol(stdlink, "write");
  fun_name = string(model_name) + "_ocp_write_solution_to_file";
  _ocp.write = (OCP_write) dlsym(_ocp.lib_handle, fun_name.c_str());
  if (dlerror() == NULL)
    MLPutString(stdlink, fun_name.c_str());
  else
    MLPutSymbol(stdlink, "False");

  return;
}

void solve()
{
  _ocp.solution.clear();
  stringstream ss;
  streambuf *backup;
  backup = cout.rdbuf();
  cout.rdbuf(ss.rdbuf());
  ss << "Print[\"Output: \n";
  _ocp.solve("ML", _ocp.data, _ocp.solution);
  ss << "\nEnd of Output\"]";
  cout.rdbuf(backup);
  cout << ss.str().c_str() << endl;
  MLEvaluateString(stdlink, (char *)ss.str().c_str());
  MLPutSymbol(stdlink, _ocp.solution["converged"].get_bool() ? "True" : "False");
  return;
}

void write_solution(char const *name)
{
  if (_ocp.solution["converged"].get_bool()) {
    _ocp.write("ML", name);
    MLPutString(stdlink, "Done");
  }
  else
    MLPutString(stdlink, "Nothing to save");
  return;
}

void load_data(char const *name)
{
  try {
    // load the defaults into the mruby interpreter's $content global var
    _ocp.data.clear();
    _ocp.mrb.load( name );
  } catch (exception e) {
#pragma GCC diagnostic ignored "-Wwrite-strings"
    MLEvaluateString(stdlink, "Print[\"Failed to open data file\"]");
#pragma GCC diagnostic warning "-Wwrite-strings"
    MLPutSymbol(stdlink, "False");
    return;
  }
  
  // move from $content into _ocp.data GenericContainer
  _ocp.mrb.global_to_GC( "$content", _ocp.data );
  
  // Push to MathLing a list corresponding to the loaded data
  get_data(_ocp.data);
  return;
}

void describe_data()
{
  stringstream ss;
  _ocp.data.print(ss);
  MLPutString(stdlink, ss.str().c_str());
  return;
}

void get_data(GenericContainer const &gc)
{
  from_gc(gc);
  return;
}

void get_data()
{
  from_gc(_ocp.data);
  return;
}

void get_solution()
{
  from_gc(_ocp.solution);
  return;
}


void set_data()
{
  _ocp.data.clear();
  to_gc(_ocp.data);
  MLPutSymbol(stdlink, "True");
  return;
}




#pragma mark - Utility functions


void to_gc(GenericContainer &gc)
{
  const char *s;
  int n;
  double d;
  
  switch (MLGetNext(stdlink)) {
    // Symbols get converted into strings, or into corresponding true/false values
    case MLTKSYM: {
      MLGetSymbol(stdlink, &s);
      if (strcmp(s, "True") == 0)
        gc.set_bool(true);
      else if (strcmp(s, "False") == 0)
        gc.set_bool(false);
      else
        gc.set_string(s);
      MLReleaseSymbol(stdlink, s);
    } break;
      
    case MLTKSTR: {
      MLGetString(stdlink, &s);
      gc.set_string(s);
      MLReleaseString(stdlink, s);
    } break;
      
    case MLTKINT: {
      MLGetInteger(stdlink, &n);
      gc.set_int(n);
    } break;
      
    case MLTKREAL: {
      MLGetReal(stdlink, &d);
      gc.set_real(d);
    } break;
      
    case MLTKFUNC: {
      MLGetFunction(stdlink, &s, &n);
      if (strcmp(s, "List") == 0) { // Curent packet is a list
        
        // If the list is empty, add to the gc an empty MAP (not a vector!)
        if (n == 0) {
#pragma GCC diagnostic ignored "-Wunused"
          map_type &m = gc.set_map();
#pragma GCC diagnostic warining "-Wunused"
          break;
        }
        
        // Save a bookmark into the incoming packet stream
        MLMARK mark = MLCreateMark(stdlink);
        int nn;
        const char *ss = "";
        // Look what function is the first entry in the incoming list
        MLGetNext(stdlink);
        if (MLGetType(stdlink) == MLTKFUNC) {
          MLGetFunction(stdlink, &ss, &nn);
        }
        MLSeekMark(stdlink, mark, 0);
        MLDestroyMark(stdlink, mark);

        // if the first entry is a Rule, then the WHOLE LIST is going to be a MAP
        if (strcmp(ss, "Rule") == 0) {
          map_type &m = gc.set_map();
          const char *key = "";
          for(int i = 0; i < n; i++){
            MLGetFunction(stdlink, &ss, &nn);
            MLGetString(stdlink, &key);
            to_gc(m[key]);
          }
          MLReleaseString(stdlink, key);
        }
        // otherwise it is going to be a VECTOR
        else {
          vector_type &v = gc.set_vector();
          v.resize(n);
          for(int i = 0; i < n; i++){
            to_gc(v[i]);
          }
        }
      }
      // it is unlikely to get here...
      else if (strcmp(s, "Rule") == 0) {
        set_data();
        set_data();
      }
      else {
        // it shall never get there!
      }
    }
      
  }
  return;
}


void from_gc(GenericContainer const &gc)
{
  switch ( gc.get_type() ) {
    case GC::GC_NOTYPE:
      MLPutSymbol(stdlink, "Null"); // not converted
      break;
    case GC::GC_POINTER:
      MLPutSymbol(stdlink, "Null"); // not converted
      break;
    case GC::GC_BOOL:
      MLPutSymbol(stdlink, gc.get_bool() ? "True" : "False");
      break;
    case GC::GC_INT:
      MLPutInteger(stdlink, gc.get_int());
      break;
    case GC::GC_REAL:
      MLPutReal(stdlink, gc.get_real());
      break;
    case GC::GC_STRING:
      MLPutString(stdlink, gc.get_string().c_str());
      break;
    case GC::GC_VEC_POINTER:
      MLPutSymbol(stdlink, "Null");
      break;
    case GC::GC_VEC_BOOL:
    { GC::vec_bool_type const & vb = gc.get_vec_bool();
      int size = (int)vb.size();
      MLPutFunction(stdlink, "List", size);
      for ( unsigned i = 0 ; i < vb.size() ; ++i )
        MLPutSymbol(stdlink, vb[i] ? "True" : "False");
    }
      break;
    case GC::GC_VEC_INT:
    { GC::vec_int_type const & vi = gc.get_vec_int();
      int size = (int)vi.size();
      MLPutFunction(stdlink, "List", size);
      for ( unsigned i = 0 ; i < vi.size() ; ++i )
        MLPutInteger(stdlink, vi[i]);
    }
      break;
    case GC::GC_VEC_REAL:
    { GC::vec_real_type const & vr = gc.get_vec_real();
      int size = (int)vr.size();
      MLPutFunction(stdlink, "List", size);
      for ( unsigned i = 0 ; i < vr.size() ; ++i )
        MLPutReal(stdlink, vr[i]);
    }
      break;
    case GC::GC_VEC_STRING:
    { GC::vec_string_type const & vs = gc.get_vec_string();
      int size = (int)vs.size();
      MLPutFunction(stdlink, "List", size);
      for ( unsigned i = 0 ; i < vs.size() ; ++i )
        MLPutString(stdlink, vs[i].c_str());
    }
      break;
    case GC::GC_VECTOR:
    { GC::vector_type const & v = gc.get_vector();
      int size = (int)v.size();
      MLPutFunction(stdlink, "List", size);
      for (GC::vector_type::const_iterator it = v.begin() ; it != v.end() ; it++) {
        from_gc(*it);
      }
    }
      break;
    case GC::GC_MAP:
    { GC::map_type const & m = gc.get_map();
      int map_length = (int)m.size();
      MLPutFunction(stdlink, "List", map_length);
      for ( GC::map_type::const_iterator it = m.begin() ; it != m.end() ; ++it ) {
        MLPutFunction(stdlink, "Rule", 2);
        from_gc(it->first);
        from_gc(it->second);
      }
    }
      break;
    default:
      MLPutSymbol(stdlink, "Null");
      break;
  }
  return;
}


int main(int argc, char* argv[])
{
	return MLMain(argc, argv);
}
