//
//  mechatronix.hh
//  ML Tests
//
//  Created by Paolo Bosetti on 13/02/14.
//  Copyright (c) 2014 Paolo Bosetti. All rights reserved.
//

#ifndef ML_Tests_mechatronix_hh
#define ML_Tests_mechatronix_hh

#include <dlfcn.h>
#define DLOPEN(l)  dlopen((l), RTLD_LAZY)
#define DLSYM(h,f) dlsym((h), (f))
#define DLCLOSE(l) dlclose((l))
#define DLERROR    dlerror()

// Mechatronix Headers
#include <MechatronixCore/MechatronixCore.hh>
#include <MechatronixSolver/MechatronixSolver.hh>
#include <MechatronixInterfaceMruby/MechatronixInterfaceMruby.hh>
#include <MechatronixInterfaceLua/MechatronixInterfaceLua.hh>

// user namespaces
#include <MechatronixCore/MechatronixCore.hh>

// Mechatronix namespaces
using namespace MechatronixCore ;
using namespace MechatronixSolver ;
using namespace MechatronixInterfaceMruby ;
using namespace GC ;

// user namespaces
using namespace MechatronixCore ;


void load_model(char const *name);
void load_data(char const *name);
void describe_data();
void from_gc(GenericContainer const &gc);
void to_gc(GenericContainer &gc);

void get_data(GenericContainer const &gc);
void set_data();
void write_solution(char const *name);
#endif
