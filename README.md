MathLink Tests
==============

Preliminary steps
-----------------

The Xcode project has a few schemes that produce MathLink-callable programs. These programs are by default put into the `bin` subfolder. The `test.nb` Mathematica notebook has minimal instructions on how to cal these programs from within Mathematica.

Before building the programs, two preliminary steps are requested. Assuming that Mathematica 9 is installed in `/Applications/Mathematica.app`, open a Terminal and type the following commands:

    $ ln -s /Applications/Mathematica.app/SystemFiles/Links/MathLink/DeveloperKit/MacOSX-x86-64/CompilerAdditions/mprep /usr/local/bin/
    $ sudo cp /Applications/Mathematica.app/SystemFiles/Links/MathLink/DeveloperKit/MacOSX-x86-64/CompilerAdditions/mathlink.framework /Library/Frameworks

Then open Xcode, select the appropriate building scheme (top left of the toolbar), and build the project (cmd-B). The result appears in the `bin` folder.

